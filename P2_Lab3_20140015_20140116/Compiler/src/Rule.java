import java.util.ArrayList;
import java.util.Scanner;

public class Rule {

	private ArrayList<String> RHSTerms = new ArrayList<String>();
	
	public Rule(String RHS){
		Scanner scanner = new Scanner(RHS);
		while(scanner.hasNext()){
			RHSTerms.add(scanner.next());
		}
		scanner.close();
	}
	
	public ArrayList<String> getRHSTerms(){
		return RHSTerms;
	}

	public String getTerm(int index){
		return RHSTerms.get(index);
	}
	
}
