
public class Main {

	public static void main(String[] args) {
		LexicalAnalyzer la = new LexicalAnalyzer();
		boolean isCorrect;
		isCorrect = la.run();
		if(isCorrect){
			SyntaxAnalyzer sa = new SyntaxAnalyzer(la.getLexemes());
			isCorrect=sa.run();
			if(isCorrect){
				System.out.println("Tree Printed Successfully");
			}
			else{
				System.out.println("Syntax Error");
			}
		}
		else{
			System.out.println("Source code has wrong tokens. Parser execution stopped !");
		}
	}
}
