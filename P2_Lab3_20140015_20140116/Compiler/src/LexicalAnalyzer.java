import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LexicalAnalyzer {

	private final String srcCodeFile = "SourceCode.txt";
	private final String tokenFile = "Tokens.txt";
	private final String lexemeFile = "Lexemes.txt"; //output file
	private ArrayList<Token> tokens = new ArrayList<Token>();
	private ArrayList<Lexeme> lexemes = new ArrayList<Lexeme>();

	public ArrayList<Lexeme> getLexemes(){
		return lexemes;
	}
	
	public boolean run(){
		String srcCode =readSourceCode();
		readTokens();
		boolean isCorrect = tokenize(srcCode);
		saveOutput();
		return isCorrect;
	}
	
	private void readTokens(){
		try {
			Scanner scanner = new Scanner(new File(tokenFile));
			while(scanner.hasNext()){
				tokens.add(new Token(scanner.next(), scanner.next()));
			}
			scanner.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	
	private String readSourceCode(){
		String sourceCode = "";
		try {
			Scanner scanner = new Scanner(new File(srcCodeFile));
			while(scanner.hasNextLine()){
				sourceCode += scanner.nextLine() + '\n';
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return sourceCode;
	}

	private boolean tokenize(String SrcCode){
		boolean isCorrect = true;
		for (int i = 0; i < SrcCode.length(); i++) {
			while (SrcCode.charAt(i) == '\t'||SrcCode.charAt(i) == ' ') {
				i++;
				if (i>=SrcCode.length()) {
					break;
				}
			}
			if (i>=SrcCode.length()) {
				break;
			}
			for (int j = 0; j < tokens.size(); j++) {
				Pattern pattern= Pattern.compile(tokens.get(j).getRegex());
				Matcher matcher= pattern.matcher(SrcCode.substring(i, SrcCode.length()));
				if(matcher.find()&&matcher.start()==0){
					String tokenName = tokens.get(j).getName();
					lexemes.add(new Lexeme(tokenName,matcher.group()));
					if(tokenName.equals("WRONG_TOKEN")){
						isCorrect = false; //Syntax error detected
					}
					i+=matcher.group().length()-1;
					break;
				}
			}
		}
		lexemes.add(new Lexeme("EOF",""));
		return isCorrect;
	}

	private void saveOutput(){
		FileWriter fw;
		PrintWriter pw;
		try {
			fw = new FileWriter(new File(lexemeFile),false);
			pw=new PrintWriter(fw);
			for (int i = 0; i < lexemes.size(); i++) {
				pw.println("<"+lexemes.get(i).getTokenName()+">: -"+lexemes.get(i).getValue() + '-');
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
