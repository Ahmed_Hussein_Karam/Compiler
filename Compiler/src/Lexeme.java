
public class Lexeme {

	private String tokenName;
	private String value;
	
	public Lexeme(String tn, String v){
		tokenName = tn;
		value = v;
	}
	public String getTokenName() {
		return tokenName;
	}
	public void setTokenName(String tokenName) {
		this.tokenName = tokenName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
