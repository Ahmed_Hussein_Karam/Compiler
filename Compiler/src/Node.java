import java.util.ArrayList;

public class Node {

	private ArrayList<Node> children = new ArrayList<Node>();
	private String value;
	private Node parent;
	public Node(String val){
		this.value = val;
	}

	public ArrayList<Node> getChildren() {
		return children;
	}
	public Node getChild(int index) {
		return children.get(index);
	}
	public void addChild(Node child) {
		this.children.add(child);
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public Node getParent() {
		return parent;
	}
	public void removeChild(Node child)
	{
		children.remove(child);
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}	
}
