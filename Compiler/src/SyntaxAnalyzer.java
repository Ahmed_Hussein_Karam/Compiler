import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class SyntaxAnalyzer {

	private final String ruleFile = "Rules.txt";
	private final String parseTableFile = "ParseTable.txt";
	private final String parseTreeFile = "ParseTree.txt"; //output file
	private final String prettyprintedcodeFile= "Pretty Printed Code.txt";
	private Node parseTreeRoot;
	private ArrayList<Lexeme> lexemes;
	private final ArrayList<Rule> rules;
	private final Hashtable<String, Integer> parseTable;
	
	public SyntaxAnalyzer(ArrayList<Lexeme> lex){
		lexemes = lex;
		//Read rules
		rules = new ArrayList<Rule>();
		Scanner scanner;
		try {
			scanner = new Scanner(new File(ruleFile));
			parseTreeRoot = new Node(scanner.nextLine());
			while (scanner.hasNext()){
				rules.add(new Rule(scanner.nextLine()));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//Read parse table
		parseTable = new Hashtable<String, Integer>();
		try {
			scanner = new Scanner(new File(parseTableFile));
			while(scanner.hasNext()){
				parseTable.put(scanner.next(), scanner.nextInt());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean run(){
		boolean isCorrect=true;
		isCorrect=constructParseTree();
		if (isCorrect){
			saveParseTree();
			ArrayList<String> terminals=organizeTerminals();
			prettyPrint(terminals);
		}
		return isCorrect;
	}
	
	public boolean constructParseTree(){
		ArrayList<Node> stack = new ArrayList<Node>();
		stack.add(parseTreeRoot);
		for (int i = 0; i < lexemes.size();) {
			while(lexemes.get(i).getTokenName().equals("M_COMMENTS")||lexemes.get(i).getTokenName().equals("S_COMMENTS")||lexemes.get(i).getTokenName().equals("EOL")) i++;
			if(parseTable.get(stack.get(stack.size()-1).getValue()+lexemes.get(i).getTokenName())==null){
				if((stack.get(stack.size()-1).getValue()).equals("E_MOVE")){
					Node parent=stack.get(stack.size()-1).getParent();
					parent.getParent().removeChild(parent);
					stack.remove(stack.size()-1);
				}
				else if((stack.get(stack.size()-1).getValue()).equals(lexemes.get(i).getTokenName())){
					stack.get(stack.size()-1).addChild(new Node(lexemes.get(i).getValue()));
					stack.remove(stack.size()-1);
					i++;
				}
				else{
					System.out.println(i);
					System.out.println(stack.get(stack.size()-1).getValue()+lexemes.get(i).getTokenName());
					return false;
				}
			}
			else{
				ArrayList<String> r =rules.get(parseTable.get(stack.get(stack.size()-1).getValue()+ lexemes.get(i).getTokenName())).getRHSTerms();
				Node current=stack.get(stack.size()-1);
				stack.remove(stack.size()-1);
				for (int j = r.size()-1; j >= 0 ; j--) {
					Node child = new Node(r.get(j));
					child.setParent(current);
					current.addChild(child);
					stack.add(child);
				}
			}
		}
		if(stack.isEmpty()){
			return true;
		}
		return false;
	}
	
	public void saveParseTree(){
		FileWriter fw =null;
		PrintWriter pw =null;
		ArrayList<Node> queue=new ArrayList<Node>();
		queue.add(parseTreeRoot);
		try {
			fw=new FileWriter(new File(parseTreeFile),false);
			pw =new PrintWriter(fw);
			
			while(!queue.isEmpty()){
				if(queue.get(0).getChildren().size()>0)
				{
				pw.print(queue.get(0).getValue()+ "-->");
				for(int j=queue.get(0).getChildren().size()-1; j>=0;j--){
					queue.add(queue.get(0).getChild(j));
					pw.print(queue.get(0).getChild(j).getValue()+"\t");
				}
				pw.print("\n");
				}
				queue.remove(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		pw.close();
	}

	public ArrayList<String> organizeTerminals(){
		ArrayList <String> terminals=new ArrayList<String>();
		ArrayList<Node> queue=new ArrayList<Node>();
		queue.add(parseTreeRoot);
		while(!queue.isEmpty()){
			if(queue.get(0).getChildren().size()>0){
				ArrayList<Node> temp=new ArrayList<Node>();
				while(queue.size()>1){
					temp.add(queue.remove(1));
				}
				for(int j=queue.get(0).getChildren().size()-1; j>=0;j--){
						queue.add(queue.get(0).getChild(j));
				}
				while(!temp.isEmpty()){
					queue.add(temp.remove(0));
				}
			}
			else{
				terminals.add(queue.get(0).getValue());
			}
			queue.remove(0);
		}
		return terminals;
	}

	public void prettyPrint(ArrayList<String> terminals){
		FileWriter fw =null;
		PrintWriter pw =null;
		try {
			fw=new FileWriter(new File(prettyprintedcodeFile),false);
			pw =new PrintWriter(fw);
			String tapping="";
			for (int i = 0; i < terminals.size()-2; i++) {
				if(terminals.get(i).equals("{")){
					pw.println(terminals.get(i));
					tapping+="\t";
					pw.print(tapping);
				}
				else if (terminals.get(i).equals(";")){
					if(terminals.get(i+1).equals("}") && tapping.length()>0)
						tapping=tapping.substring(0,tapping.length()-1);
					pw.println(terminals.get(i));
					pw.print(tapping);
				}
				else if(terminals.get(i).equals("}")){
					if(terminals.get(i+1).equals("}") && tapping.length()>0)
						tapping=tapping.substring(0,tapping.length()-1);
					pw.println(terminals.get(i));
					pw.print(tapping);
				}
				else{
					if(terminals.get(i+1).equals("}") && tapping.length()>0)
						tapping=tapping.substring(0,tapping.length()-1);
					if(!terminals.get(i+1).equals(";"))
						pw.print(terminals.get(i)+" ");
					else pw.print(terminals.get(i));
				}
			}
			pw.print(terminals.get(terminals.size()-2));
		} catch (IOException e) {
			e.printStackTrace();
		}
		pw.close();
	}
}
