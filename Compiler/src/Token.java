
public class Token {

	private String name;
	private String regex;
	Token(String n, String r){
		name=n;
		regex=r;
	}
	public String getName() {
		return name;
	}

	public void setName(String n) {
		this.name = n;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String r) {
		this.regex = r;
	}
}
